This Module allows user control the privacy of selected entity fields.

It can be extended for other modules like user_relationships. See the api file for hooks

* Based on http://drupal.org/project/user_field_privacy
* Uses https://github.com/marghoobsuleman/ms-Dropdown to display image icons
