<?php


/**
 * @file
 * User Relationships admin settings and config forms
 */

/**
 * Main settings
 */
function entity_relationships_admin_settings() {

  $form = array();
  $form['general'] = array(
    '#type'   => 'fieldset',
    '#title'  => t('General'),
    '#weight' => -10,
    '#group' => 'settings',
  );
  $form['general']['entity_field_privacy_default_realm'] = array(
    '#type'           => 'radios',
    '#title'          => t('Default realm'),
    '#description'    => t('The default realm for new fields.'),
    '#options'        => entity_field_privacy_options(),
    '#default_value'  => variable_get('entity_field_privacy_default_realm', 'public'),
  );

  $entity_infos = entity_get_info();
  // Build an options array of entity info
  $options = array();
  foreach ($entity_infos as $entity_type => $entity_info) {
    foreach ($entity_info['bundles'] as $bundle_name => $bundle_info) {
      $options[$entity_info['label']][$entity_type.':'.$bundle_name] = $bundle_info['label'];
    }
  }

  $form['general']['entity_field_privacy_entity_types'] = array(
    '#type'           => 'select',
    '#title'          => t('Entity types'),
    '#description'    => t('Select the entity types you want to use field privacy with'),
    '#options'        => $options,
    '#size'           => 15,
    '#multiple'       => TRUE,
    '#default_value'  => variable_get('entity_field_privacy_entity_types', ''),
  );

  $infos = _entity_field_privacy_get_info(null, null);

  $form['files'] = array(
    '#type'        => 'fieldset', 
    '#title'       => t('Custom Icons'),
    '#description' => t('Icon files must be uploaded in JPG, GIF or PNG format and at least 13x14 pixels in size.'),
    '#collapsible' => FALSE, 
    '#collapsed'   => FALSE,
  );

  foreach ($infos as $realm => $info) {
    $field_name = $realm . '_icon_file';
    $form['files'][$field_name] = array(
      '#title' => t('@label Icon', array('@label' => $info['label'])),
      '#type' => 'managed_file',
      '#default_value' => variable_get($field_name . '_fid', null), // Use different variable name for the variable, so it won't be overridden by system submit handler
      '#upload_location' => 'public://field-privacy-icons/',
      '#upload_validators' => array(
        'file_validate_extensions' => array('jpg jpeg gif png'),
        'file_validate_image_resolution' => array('13x14', '13x14')
      )
    );
  }

  // Perform our custom submit before system submit
  $form['#submit'][] = '_entity_field_privacy_settings_upload_handler';

  return system_settings_form($form);
}

function _entity_field_privacy_settings_upload_handler($form, &$form_state) {
  global $user;
  $infos = _entity_field_privacy_get_info(null, null);
  $files_updated = false;
  foreach ($infos as $realm => $info) {
    $field_name = $realm . '_icon_file';
    // Load the file via file.fid.
    $file = file_load($form_state['values'][$field_name]);
    if ($file) {
      // Change status to permanent.
      $file->status = FILE_STATUS_PERMANENT;
      // Save.
      file_save($file);
      // Save file to variable
      variable_set($field_name . '_fid', $file->fid);
      // Record that the module is using the file. 
      file_usage_add($file, 'entity_field_privacy', 'user', $user->uid);
      // Unset formstate value
      unset($form_state['values'][$field_name]); // make sure it is unset for system submit
      $files_updated = true;
    } else {
      $fid = variable_get($field_name . '_fid', null);
      if (!empty($fid)) {
        // If there had been a file, make sure it's deleted
        $file = file_load($fid);
        if ($file) {
          file_delete($file);
        }
        $files_updated = true;
      }
      variable_set($field_name . '_fid', null);
    }
  }
  if ($files_updated) {
    cache_clear_all('entity_field_privacy_info', 'cache');
  }
}
