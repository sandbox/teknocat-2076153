<?php

/**
 * Implementation of hook_entity_field_privacy_info_alter
 * 
 * Can be used to change icons and captions
 * @param array $infos
 * @return string 
 */
function hook_user_relationships_entity_field_privacy_info_alter(&$infos){
  $infos['public']['label'] = "Bad Luck Brian";
  return $infos;
}


/**
 * Implementation of hook_entity_field_privacy_info(). The key is the realm.
 * If your privacy realm is only applicable to a specific entity type/bundle, you can add logic to this
 * function to determine whether or not to return anything based on the passed in values. Keep in mind, however,
 * that when the configuration form is displayed, the entity_type and bundle arguments will be empty, so you should
 * always return a valid value in that case.
 * 
 * You can call the _entity_field_privacy_bundle_allowed() function to determine if 
 * @param string $entity_type
 * @param string $bundle
 * @return array An array with field definitions keyed by realm 
 */
function hook_entity_field_privacy_info($entity_type, $bundle) {
  $info = array();
  if (_entity_field_privacy_bundle_allowed('node', 'some_special_node_type', $entity_type, $bundle)) { // remove this if your privacy option is allowed for all bundles
    $info['my_module'] = array(
      'label' => t('My Bitches'),
      'icon' => '/' . drupal_get_path('module', 'mymodule') . '/bitch.gif',
      'description' => t("My bitches can see it"),
    //  'access callback' => 'my_custom_callback_name' default callback name is used if this is left out
    );
  }
  return $info;
}

/**
 * Sample default callback for realm my_module
 * @param type $field the fiedl being accessed
 * @param type $uid the uid of the account being accessed
 * @param type $account the acting user
 * @return boolean 
 */
function entity_field_privacy_access_my_module($field, $uid, $account){
  
  return my_module_account_is_bitch_of_uid($uid, $account);
}